# Headless testing example with Jasmine, PhantomJS and Grunt

Accompanies blog post - http://simonsmith.io/headless-testing-with-jasmine-phantomjs-and-grunt

	bower install
	npm install
	npm test

setup

    npm install -g grunt-cli
    npm install grunt-contrib-jasmine --save-dev
    bower install jquery jasmine-jquery --save-dev
    bower install jasmine-ajax --save-dev

#### articles

* What is the purpose of mock objects? http://stackoverflow.com/questions/3622455/what-is-the-purpose-of-mock-objects
* AJAX testing with Jasmine 2.0 – 02 http://theaveragedev.com/ajax-testing-with-jasmine-2-02/
* Jasmine 2.0 how to handle ajax requests http://stackoverflow.com/questions/22245991/jasmine-2-0-how-to-handle-ajax-requests
* What's the difference between faking, mocking, and stubbing? http://stackoverflow.com/questions/346372/whats-the-difference-between-faking-mocking-and-stubbing

#### terms

* fake - implents an interface but contains fixed data and no logic
* stub - provide canned answeres to calls, not responding to anything outside the test
* mock - simulate the behaviour of complex objects
