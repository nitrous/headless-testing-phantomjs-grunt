jasmine.getFixtures().fixturesPath = 'test/index';

describe('index page', function() {
  var elem;

  beforeEach(function() {
    loadFixtures('index.html');
    elem = $('footer');
  });

  it('should add a class to the footer element', function() {
    elem.addClass("footer")
    expect(elem).toHaveClass('footer');
  })

  it('should add a link footer element', function() {
    elem.append("<a href=''>link</a>")
    expect(elem).toContainElement('a');
  })

})
